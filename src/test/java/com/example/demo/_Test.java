import org.example.TreeUniversity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class _Test
{
    static TreeUniversity treeUniversity;
    @BeforeAll
    public static void init(){
        treeUniversity=new TreeUniversity();

    }
    @Test
    protected void testUniversity(){
        TreeUniversity.Node all=new TreeUniversity.Node("NaUKMA");
        TreeUniversity.Node faculty1=new TreeUniversity.Node("Fi");
        TreeUniversity.Node student1=new TreeUniversity.Node("John Makkein");
        TreeUniversity.Node student2=new TreeUniversity.Node("Paul Vein");
        TreeUniversity.Node faculty2=new TreeUniversity.Node("FP");
        TreeUniversity.Node student3=new TreeUniversity.Node("Klein");
        treeUniversity.addEdge(all,faculty1);
        treeUniversity.addEdge(all,faculty2);
        treeUniversity.addEdge(faculty2,student2);
        treeUniversity.addEdge(faculty1,student1);
        treeUniversity.addEdge(faculty2,student3);
        Assertions.assertEquals(treeUniversity.framework.get(all).contains(faculty1),true);


    }
}
