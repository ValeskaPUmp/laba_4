package org.example;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class TreeUniversity {
    public static class Node{

        String name;
        boolean visited;
        public Node(String name){

            this.name=name;
            visited=false;
        }
        File info;
        public void visit(){
            visited=true;
        }
        public void unvisit(){
            visited=false;
        }

        @Override
        public String toString() {
            return name;
        }
    }
    public HashMap<Node,LinkedList<Node>> framework;
    public TreeUniversity(){
        framework=new HashMap<>();
    }
    public void addEdge(Node source,Node destination){
        if(!framework.keySet().contains(source)){
            framework.put(source,null);
        }
        if(!framework.keySet().contains(destination)){
            framework.put(destination,null);
        }
        LinkedList<Node> temp=framework.get(source);
        if(temp!=null){
            temp.remove(destination);
        }else temp=new LinkedList<>();
        temp.add(destination);
        framework.put(source,temp);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    private String DFSresult="";

    public void resetAll(){
        for(Node n:framework.keySet()){
            n.unvisit();
        }
    }
}
